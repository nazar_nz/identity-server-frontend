import {Injectable} from '@angular/core';
import {HttpService} from '../services/http.service';
import {Observable, of} from 'rxjs';
import {map} from 'rxjs/internal/operators';

@Injectable()
export class CountriesProvider {

  countries: Array<Country> = [];
  observable: Observable<any>;

  constructor(private http: HttpService) {}

  getCountries(): Observable<any> {
    if (this.countries.length > 0) {
      return of(this.countries);
    } else if (this.observable) {
      return this.observable;
    } else {
      this.observable = this.http.get('/api/countries?size=300').pipe(
        map((res: any) => res.content)
      );
      this.observable.subscribe((data: any) => {
        this.countries = data;
      });
      return this.observable;
    }
  }
}
