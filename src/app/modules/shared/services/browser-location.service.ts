import { Injectable } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BrowserLocationService {
  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) { }

  getDataFromLocation(dataKey: string): any {
    let routeData = false;
    this.route.queryParams.subscribe((data) => {
      if (data[dataKey]) {
        routeData = data[dataKey];
      }
    });
    return routeData;
  }

  addDataToLocation(dataKey: string, value: string) {
    const params = {};

    this.route.queryParams.subscribe((data) => {
      Object.keys(data).forEach((e) => {
        params[e] = data[e];
      });
    });

    if (value !== '') {
      params[dataKey] = value;
    } else {
      delete params[dataKey];
    }

    return this.router.navigate([], {
      relativeTo: this.route,
      queryParams: params,
      replaceUrl: true
    });
  }

  public addPaginationParams(size: number, page: number) {
    let result = {};
    this.route.queryParams.subscribe((data) => {
      result = { ...data };
    });
    result['page'] = page;
    result['size'] = size;
    return this.router.navigate([], {
      relativeTo: this.route,
      queryParams: result,
      replaceUrl: true
    });
  }
}
