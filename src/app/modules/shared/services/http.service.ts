import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient, private route: ActivatedRoute) { }

  public get(url: string) {
    let httpParams = {};
    this.route.queryParams.subscribe((data) => {
       httpParams = data;
    });
    return this.http.get(url, { params: httpParams });
  }

  public post(url: string, payload: any) {
    return this.http.post(url, payload);
  }

  public put(url: string, payload: any) {
    return this.http.put(url, payload);
  }

  public patch(url: string) {

  }
}
