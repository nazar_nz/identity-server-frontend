import { Injectable } from '@angular/core';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class DateMomentJsService {

  private moment: any = moment;

  constructor() {}

  public getMoment (): any {
    return this.moment;
  }

  public create(date: string, format?: string) {
    if (format) {
      return this.moment(date, format);
    }
    return this.moment(date);
  }
}
