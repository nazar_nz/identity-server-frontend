import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl} from '@angular/forms';
import {BrowserLocationService} from '../../services/browser-location.service';

@Component({
  selector: 'app-filter-text',
  templateUrl: './filter-text.component.html',
  styleUrls: ['./filter-text.component.css']
})
export class FilterTextComponent implements OnInit {

  @Input() filterKey: string;
  @Input() placeholder: string;
  @Input() style: any = {};
  @Output() eventEmitter: EventEmitter<any> = new EventEmitter();
  public value: FormControl = new FormControl('');

  constructor(private browserLocation: BrowserLocationService) {}

  ngOnInit() {
    const value = this.browserLocation.getDataFromLocation(this.filterKey);
    if (value) {
      this.value.setValue(value);
      this.eventEmitter.emit();
    }
  }

  resetFilter() {
    this.value.setValue('');
    this.doFilter('');
  }

  doFilter(value) {
    this.browserLocation.addDataToLocation(this.filterKey, value).then(() => {
      this.eventEmitter.emit();
    });
  }
}
