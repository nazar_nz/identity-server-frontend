import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormControl} from '@angular/forms';
import {DateMomentJsService} from '../../services/date-momentjs.service';
import {BrowserLocationService} from '../../services/browser-location.service';

@Component({
  selector: 'app-filter-date',
  templateUrl: './filter-date.component.html',
  styleUrls: ['./filter-date.component.css']
})
export class FilterDateComponent implements OnInit {

  @Input() filterKey: string;
  @Input() placeholder: string;
  @Output() eventEmitter: EventEmitter<any> = new EventEmitter();

  public value: FormControl = new FormControl('');

  constructor(
    private browserLocation: BrowserLocationService,
    private route: ActivatedRoute,
    private router: Router,
    private momentJs: DateMomentJsService
  ) {}

  ngOnInit() {
    const date = this.browserLocation.getDataFromLocation(this.filterKey);
    if (date) {
      const dateString = this.momentJs
        .create(date, 'YYYY-MM-DD')
        .format('YYYY-MM-DD');
      this.value.setValue(dateString);
      this.eventEmitter.emit();
    }
  }

  resetFilter() {
    this.value.setValue('');
    this.doFilter('');
  }

  doFilter(value) {
    if (value !== '') {
      value = this.momentJs.create(value).format('YYYY-MM-DD');
    }
    this.browserLocation.addDataToLocation(this.filterKey, value).then(() => {
      this.eventEmitter.emit();
    });

  }

}
