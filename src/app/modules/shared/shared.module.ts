import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from '@angular/cdk/layout';
import { HttpService } from './services/http.service';
import { CountriesProvider} from './providers/language.provider';
import { FilterTextComponent } from './components/filter-text/filter-text.component';
import { FilterDateComponent } from './components/filter-date/filter-date.component';
import { DateMomentJsService } from './services/date-momentjs.service';
import { BrowserLocationService } from './services/browser-location.service';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material/material.module';

@NgModule({
  declarations: [
    FilterTextComponent,
    FilterDateComponent
  ],
  providers: [
    HttpService,
    DateMomentJsService,
    BrowserLocationService,
    CountriesProvider,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    LayoutModule,
  ],
  exports: [
    MaterialModule,
    ReactiveFormsModule,
    FilterTextComponent,
    FilterDateComponent
  ]
})
export class SharedModule { }
