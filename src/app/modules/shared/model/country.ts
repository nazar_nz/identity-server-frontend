
declare interface Country {
  iso3: string;
  iso2: string;
  name: string;
}
