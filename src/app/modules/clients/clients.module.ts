import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientsListComponent } from './clients-list/clients-list.component';
import { ClientsDetailsComponent } from './clients-details/clients-details.component';
import { RouterModule, Routes } from '@angular/router';

const appRoutes: Routes = [
  { path: 'clients', component: ClientsListComponent },
  { path: 'clients/:id', component: ClientsDetailsComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(appRoutes)
  ],
  declarations: [ClientsListComponent, ClientsDetailsComponent]
})
export class ClientsModule { }
