import {Component, OnInit} from '@angular/core';
import {UserService} from '../services/user.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrManager} from 'ng6-toastr-notifications';

@Component({
  selector: 'app-users-edit',
  templateUrl: './users-edit.component.html',
  styleUrls: ['./users-edit.component.css']
})
export class UsersEditComponent implements OnInit {

  private loading = true;
  private user: User;

  constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrManager
  ) { }

  ngOnInit() {

    this.userService.getUser(+this.route.snapshot.params['id']).subscribe((data: User) => {
      this.loading = false;
      this.user = data;
    });
  }

  saveHandler(value) {
    this.userService.updateUser(+this.route.snapshot.params['id'], value).subscribe((data: any) => {
      this.toastr.successToastr('User is saved!', 'Save success');
      this.router.navigate(['/users']);
    });
  }

}
