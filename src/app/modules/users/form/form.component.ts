import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {CountriesProvider} from '../../shared/providers/language.provider';

@Component({
  selector: 'app-user-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class UserFormComponent implements OnInit {

  @Input() user: User;
  @Output() saveHandler: EventEmitter<object> = new EventEmitter();
  userForm: FormGroup;
  countries: Array<Country>;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private countryProvider: CountriesProvider
  ) { }

  ngOnInit() {
    this.userForm = this.fb.group({
      firstName: [this.user.firstName, Validators.required],
      lastName: [this.user.lastName, Validators.required],
      patronymic: [this.user.patronymic, Validators.required],
      email: [this.user.email, Validators.required],
      country: [this.user.country, Validators.required]
    });

    this.countryProvider.getCountries().subscribe((data: any) => {
      this.countries = data;
    });
  }

  saveUser(value) {
    this.saveHandler.emit(value);
  }

  cancelEdit() {
    this.router.navigate(['/users']);
  }
}
