import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {UserService} from '../services/user.service';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {BrowserLocationService} from '../../shared/services/browser-location.service';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit, AfterViewInit {

  private timeout: any;
  private dataSource = new MatTableDataSource();
  private totalLength = 0;
  private loading = true;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private userService: UserService,
    private browserLocation: BrowserLocationService
  ) { }

  ngOnInit() {
    this.timeout = null;
  }

  ngAfterViewInit(): void {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    this.initPagination();
    this.updateData();
  }

  changePage(event) {
    console.log(event);
    this.browserLocation.addPaginationParams(
      event.pageSize,
      event.pageIndex
    ).then(() => {
        this.updateData();
    });
  }

  updateData() {
    this.userService.getUsers().subscribe((res: any) => {
      this.dataSource.data = res.content;
      this.totalLength = res.totalElements;
      this.loading = false;
    });
  }

  updateDataWithTimeout() {
    const self = this;
    if (self.timeout !== null) {
      clearTimeout(self.timeout);
    }
    self.timeout = setTimeout(function() {
      self.updateData();
    }, 800);
  }

  tableSort($event) {
    const sortParam = [$event.active, $event.direction];
    let then: Promise<boolean> = null;
    if ($event.direction !== '') {
      then = this.browserLocation.addDataToLocation('sort', sortParam.join(',') );
    } else {
      then = this.browserLocation.addDataToLocation('sort', '' );
    }
    then.then( () => {
      this.updateData();
    });
  }

  initPagination() {
    this.browserLocation.addPaginationParams(this.paginator.pageSize, this.paginator.pageIndex);
  }

  getPageSize() {
    return this.browserLocation.getDataFromLocation('size') || 20;
  }
}
