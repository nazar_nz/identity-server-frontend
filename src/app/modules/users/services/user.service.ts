import { Injectable } from '@angular/core';
import {HttpService} from '../../shared/services/http.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpService) { }
  getUsers() {
    return this.http.get('/api/users');
  }

  getUser(id: number) {
    return this.http.get('/api/users/' + id);
  }

  updateUser(id: number, payload ) {
    return this.http.put('/api/users/' + id , payload);
  }

}
