
declare interface User {
  id: number;
  uuid: string;
  email: string;
  firstName: string;
  lastName: string;
  patronymic: string;
  country: string;
  language: string;
  avatarUrl: string;
  createdAt: string;
  updatedAt: string;
}
