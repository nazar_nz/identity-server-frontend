import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { UsersListComponent } from './users-list/users-list.component';
import { UsersDetailsComponent } from './users-details/users-details.component';
import { UserFormComponent } from './form/form.component';
import { UsersCreateComponent } from './users-create/users-create.component';
import { UsersEditComponent } from './users-edit/users-edit.component';

const appRoutes: Routes = [
  { path: 'users', component: UsersListComponent },
  { path: 'users/:id', component: UsersDetailsComponent },
  { path: 'users/:id/create', component: UsersCreateComponent },
  { path: 'users/:id/edit', component: UsersEditComponent },
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(appRoutes)
  ],
  declarations: [
    UsersListComponent,
    UsersDetailsComponent,
    UserFormComponent,
    UsersCreateComponent,
    UsersEditComponent
  ],
  exports: []
})
export class UsersModule { }
